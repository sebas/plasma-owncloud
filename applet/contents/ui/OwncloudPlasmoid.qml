/***************************************************************************
 *                                                                         *
 *   Copyright 2012-2016 Sebastian Kügler <sebas@kde.org>                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import QtQuick 2.2
import QtQuick.Layouts 1.1

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import org.kde.plasma.owncloud 1.0


Item {
    id: root
    objectName: "root"

    Layout.minimumWidth: units.gridUnit * 14
    Layout .minimumHeight: units.gridUnit * 10

    Plasmoid.switchWidth: units.gridUnit * 14
    Plasmoid.switchHeight: units.gridUnit * 10
    Plasmoid.icon: "owncloud"
    Plasmoid.toolTipMainText: i18n("Cloud Synchronization")
    Plasmoid.status: PlasmaCore.Types.ActiveStatus
    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation

    property bool webdavInFileManager: false
    property bool showRemoveFolder: false

    //anchors.margins: units.gridUnit

    OwncloudSettings {
        id: owncloudSettings
        objectName: "owncloudSettings"
        signal updateGlobalStatus(int s)
        // Update the popup icon
        onGlobalStatusChanged: updateGlobalStatus(owncloudSettings.globalStatus)
    }

    DirectoryLister { id: dir }

    Plasmoid.compactRepresentation: Component {
        PlasmaCore.IconItem {
            source: plasmoid.icon
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.MiddleButton
                onClicked: {
                    if (mouse.button == Qt.MiddleButton) {
                        //root.playPause()
                    } else {
                        plasmoid.expanded = !plasmoid.expanded
                    }
                }
            }
        }
    }

    Plasmoid.fullRepresentation: Item {
        id: owncloudItem

        Layout.preferredWidth: units.gridUnit * 30
        Layout.preferredHeight: units.gridUnit * 20
        //anchors { fill: parent; margins: units.largeSpacing; }
        PlasmaExtras.Heading {
            id: headingLabel
            level: 2
            text: {
                if (owncloudSettings.owncloudStatus != OwncloudSettings.Connected) {
                    return i18n("File Synchronization");
                } else {
                    var u = owncloudSettings.url;
                    u = u.replace("https://", "");
                    u = u.replace("http://", "");
                    return u;
                }
            }
            anchors { top: parent.top; left: parent.left; right: parent.right; }
        }

        FolderList {
            id: folderList
            width: parent.width
            //clip: true
            height: units.gridUnit * 20
            visible: owncloudSettings.owncloudStatus == OwncloudSettings.Connected
            anchors {
                top: headingLabel.bottom; topMargin: units.largeSpacing; left: parent.left; right: parent.right; bottom: enabledSwitch.top; }
        }

        ProgressItem {
            id: progressItem
            progress: owncloudSettings.progress
            anchors {
                bottom: enabledSwitch.top;
                left: folderList.left;
                right: folderList.right;
                bottomMargin: units.gridUnit * 2
            }
            height: childrenRect.height
            visible: owncloudSettings.globalStatus == OwncloudFolder.Running
        }
        ErrorHandler {
            id: ocStatus
            anchors { top: folderList.top; left: folderList.left; right: folderList.right; }
            visible: owncloudSettings.owncloudStatus != OwncloudSettings.Connected
        }
        PlasmaComponents.CheckBox {
            id: enabledSwitch
            text: i18n("Enable")
            visible: folderList.count > 0
            checked: owncloudSettings.globalStatus != OwncloudFolder.Disabled
            anchors { bottom: parent.bottom; left: parent.left; rightMargin: units.largeSpacing }
            onClicked: owncloudSettings.enableAllFolders(checked)
        }
        PlasmaComponents.ToolButton {
            id: addSyncFolderButton
            text: i18n("Settings...")
            iconSource: "configure"
            visible: owncloudSettings.owncloudStatus == OwncloudSettings.Connected
            anchors { bottom: parent.bottom; right: parent.right; rightMargin: units.largeSpacing; }
            onClicked: {
                owncloudSettings.openConfig();
            }
        }
    }
}

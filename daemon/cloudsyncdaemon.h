/***************************************************************************
 *                                                                         *
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef CLOUDSYNCDAEMON_H
#define CLOUDSYNCDAEMON_H

#include <QObject>
#include <QCoreApplication>

class CloudSyncDaemon : public QObject
{
    Q_OBJECT

    public:
        CloudSyncDaemon( QObject* parent = 0 );
        virtual ~CloudSyncDaemon();

        static CloudSyncDaemon* self();

    public Q_SLOTS:
        void quit();

    private:
        void init();

        enum State {
            StateDisabled,
            StateEnabled,
            StateDisabling,
            StateEnabling
        };
        State m_currentState;

        static CloudSyncDaemon* s_self;
};

#endif

/***************************************************************************
 *                                                                         *
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "cloudsyncdaemon.h"
#include "kfilesync.h"

#include "cloudsyncadaptor.h"

#include <QDBusConnection>


CloudSyncDaemon* CloudSyncDaemon::s_self = 0;

CloudSyncDaemon::CloudSyncDaemon( QObject* parent )
    : QObject( parent ),
      m_currentState(StateDisabled)
{
    s_self = this;

    init();
}


CloudSyncDaemon::~CloudSyncDaemon()
{
    QDBusConnection::sessionBus().unregisterService( "org.kde.cloudsync" );
}


void CloudSyncDaemon::init()
{
    CloudSync* syncdaemon = new KFileSync(this);
    //syncdaemon->init();
    //OwncloudSync* syncdaemon = new OwncloudSync(this);
    new CloudsyncAdaptor(syncdaemon);

    QDBusConnection::sessionBus().registerService( "org.kde.cloudsync" );

    QDBusConnection::sessionBus().registerObject( "/", syncdaemon);
}

void CloudSyncDaemon::quit()
{
    QCoreApplication::instance()->quit();
}

CloudSyncDaemon* CloudSyncDaemon::self()
{
    return s_self;
}

#include "cloudsyncdaemon.moc"

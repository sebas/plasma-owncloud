project(cloudsyncd)

message(status "OWNCLOUD INCLUDES ${OWNCLOUD_INCLUDES}")

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${OWNCLOUD_INCLUDES}
  )

set(cloudsyncd_SRCS
  cloudsync.cpp
  cloudsyncdaemon.cpp
  kfilesync.cpp
  main.cpp
  )

qt5_add_dbus_adaptor(cloudsyncd_SRCS
  org.kde.cloudsync.xml
  cloudsync.h
  CloudSync)

add_executable(cloudsyncd ${cloudsyncd_SRCS})

ADD_LIBRARY(cloudsyncd_SRCS STATIC IMPORTED)

target_link_libraries(cloudsyncd
    Qt5::Core
    Qt5::DBus
    Qt5::Qml
    Qt5::Widgets
    KF5::KIOCore
    ${OWNCLOUD_LIBRARIES}
    ${ACCOUNTSQT_LIBRARIES}
    ${SIGNONQT_LIBRARIES}
    KAccounts
)


install(
  FILES cloudsyncd.desktop
  DESTINATION ${AUTOSTART_INSTALL_DIR})

install(
  TARGETS cloudsyncd
  ${INSTALL_TARGETS_DEFAULT_ARGS})

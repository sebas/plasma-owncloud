/***************************************************************************
 *                                                                         *
 *   Copyright 2012-2016 Sebastian Kügler <sebas@kde.org>                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

// Own
#include "cloudsync.h"
#include "owncloudfolder.h"
#include "owncloudsettings.h"

//#include <KIO/AccessManager>
#include <QDebug>
#include <QVariant>
#include <QTimer>

// KAccounts

#include <KAccounts/core.h>
#include <KAccounts/getcredentialsjob.h>
#include <Accounts/Service>
#include <Accounts/Manager>
#include <Accounts/AccountService>


#include <syncresult.h>
#include "folder.h"
#include <accountmanager.h>
#include <accountstate.h>
#include <configfile.h>
#include <progressdispatcher.h>

Q_LOGGING_CATEGORY(POC_SYNC, "plasma.owncloud.sync");

using namespace OCC;

// Hack: FolderMan's ctor is private, OCC::Application is Friend
namespace OCC {
    class Application : public QObject
    {
        Q_OBJECT
    public:
        explicit Application(QObject *parent = 0)
        : QObject(parent)
        {
            _folderManager.reset(new FolderMan);
        }
        virtual ~Application() {};
        QScopedPointer<FolderMan> _folderManager;
    };
};

class OwncloudSyncPrivate {
public:
    CloudSync *q;
    QString name;
    QString localPath;
    QString remotePath;
    QString display;

    QVariantMap folderList;
    QVariantMap owncloudInfo;
    QHash<QString, QVariantMap> folders;
    QHash<QString, QDateTime> syncTime;

    OCC::FolderMan* folderMan;
    QScopedPointer<FolderMan> _folderManager;

//     OCC::ownCloudInfo* ocInfo;
    QString configHandle;
    QTimer *delay;
    int ocStatus;
    int ocError;

    int c;
};

CloudSync::CloudSync(QObject *parent)
    : QObject(parent)
{
    d = new OwncloudSyncPrivate;
    d->q = this;
    d->display = "";
    d->delay = 0;
    d->ocStatus = OwncloudSettings::Disconnected;
    d->ocError = OwncloudSettings::NoError;
//     d->ocInfo = OCC::ownCloudInfo::instance();
// //     KIO::AccessManager *nam = new KIO::AccessManager(this);
// //     qCDebug(POC_SYNC) << "OC Seetting KIO::NAM";
// //     d->ocInfo->setNetworkAccessManager(nam);
    init();

    slotFetchCredentials();
}

void CloudSync::init()
{

    new Application(this); // Initializes FolderMan instance

    AccountManager::instance()->restore();

    QList<AccountStatePtr> acl = AccountManager::instance()->accounts();
    qCDebug(POC_SYNC) << "Accounts" << acl.count();


    for (auto ac : acl) {
        connect(ac.data(), &AccountState::stateChanged,
                this, &CloudSync::accountStateChanged);

        ac->checkConnectivity();
        qCDebug(POC_SYNC) << "AS: " << ac->isConnected() << ac->stateString(ac->state());
    }
    qCDebug(POC_SYNC) << "accounts #: " << acl.count();



    d->folderMan = OCC::FolderMan::instance();

    qCDebug(POC_SYNC) << "folderMan: " << d->folderMan->map().count();

    connect(d->folderMan, &FolderMan::folderSyncStateChange,
             this, &CloudSync::slotSyncStateChange);

    FolderMan::instance()->setSyncEnabled(true);
    FolderMan::instance()->setupFolders();

//     qCDebug(POC_SYNC) << "FFM " << FolderMan::instance()->map();
//     for (auto f : FolderMan::instance()->map()) {
//         qCDebug(POC_SYNC) << "  F: " << f->alias() << f->path() << f->remotePath();
//     }
//     loadFolders();
    //d->ocInfo->setCustomConfigHandle("mirall");
//     connect(d->ocInfo,SIGNAL(ownCloudInfoFound(QString,QString,QString,QString)),
//              SLOT(slotOwnCloudFound(QString,QString,QString,QString)));
//
//     connect(d->ocInfo,SIGNAL(noOwncloudFound(QNetworkReply*)),
//              SLOT(slotNoOwnCloudFound(QNetworkReply*)));
//
//     qCDebug(POC_SYNC) << "OC connecting to slotAuthCheck";
//     connect(d->ocInfo,SIGNAL(ownCloudDirExists(QString,QNetworkReply*)),
//              this,SLOT(slotAuthCheck(QString,QNetworkReply*)));
//
//     connect(d->ocInfo, SIGNAL(ownCloudDirExists(QString,QNetworkReply*)),
//              SLOT(slotDirCheckReply(QString,QNetworkReply*)));


//     connect(OCC::ProgressDispatcher::instance(),
//             SIGNAL(itemProgress(int, const QString&, const QString&, qint64, qint64)),
//             this,
//             SLOT(itemProgress(int, const QString&, const QString&, qint64, qint64)));

    connect(OCC::ProgressDispatcher::instance(),
            &ProgressDispatcher::progressInfo,
            this,
            &CloudSync::progressInfo);

}

CloudSync::~CloudSync()
{
    delete d;
}

OCC::FolderMan* CloudSync::folderMan()
{
    return d->folderMan;
}

// OCC::ownCloudInfo* OwncloudSync::ocInfo()
// {
//     return d->ocInfo;
// }
//
void CloudSync::slotSyncStateChange(OCC::Folder *folder)
{
    if (!folder) {
        qCDebug(POC_SYNC) << "Folder is null";
        return;
    }
    qCDebug(POC_SYNC) << "Folder sync state change" << folder->alias();
    if (folder && (folder->syncResult().status() == OCC::SyncResult::Success)) {
        d->syncTime[folder->alias()] = QDateTime::currentDateTime();
//         qCDebug(POC_SYNC) << "OC updated syncTime for " << s << d->syncTime[s];
    }
    updateFolder(folder);
}

// void OwncloudSync::itemProgress(int kind, const QString& folder, const QString& file, qint64 p1, qint64 p2)
// {
//     qCDebug(POC_SYNC) << "POC Item progress: " << folder << file << p1 << p2;
// }
//
// void OwncloudSync::overallProgress(const QString& folder, const QString& file, int fileNo, int fileCnt, qint64 p1, qint64 p2)
// {
// }
//
void CloudSync::progressInfo(const QString& folder, const OCC::ProgressInfo& progress)
{
//     qCDebug(POC_SYNC) << "POC Overall Progress: " << folder << file << fileNo << fileCnt << p1 << p2;
    QVariantMap vm;
//     vm["folder"] = folder;
//     vm["file"] = file;
//     vm["fileNo"] = fileNo;
//     vm["fileCnt"] = fileCnt;
//     vm["p1"] = p1;
//     vm["p2"] = p2;
    emit progressChanged(vm);

}


QString CloudSync::display()
{
    //qCDebug(POC_SYNC) << "OC display() " << d->display;
    return d->display;
}

QVariantMap CloudSync::folder(QString name)
{
    //qCDebug(POC_SYNC) << "OC folder()" << name;
    return d->folders[name];
}

void CloudSync::enableFolder(const QString &name, bool enabled)
{
    qCDebug(POC_SYNC) << " OC enableFolder: " << name << enabled;
    OCC::Folder *f = d->folderMan->folder(name);
    if (f) {
        f->setSyncPaused(enabled);
        updateFolder(f);
    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

void CloudSync::syncFolder(const QString& name)
{
    qCDebug(POC_SYNC) << " OC syncFolder: " << name;
    OCC::Folder *f = d->folderMan->folder(name);
    if (f) {
        f->startSync(QStringList());

    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

void CloudSync::cancelSync(const QString& name)
{
    qWarning() << "OC Terminating a sync operation is not implemented yet.";
    //qCDebug(POC_SYNC) << " OC syncFolder: " << name;
    OCC::Folder *f = d->folderMan->folder(name);
    if (f) {
        qWarning() << "Terminating a sync operation is not implemented yet.";
    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

QVariantMap CloudSync::folderList()
{
    return d->folderList;
}

void CloudSync::refresh()
{
    qCDebug(POC_SYNC) << "POC Syncdaemon refresh()";
    auto accountList = AccountManager::instance()->accounts();
    bool isConfigured = (!accountList.isEmpty());
    // see also owncloud-client/src/src/gui/owncloudgui.cpp:341:...
    if (!isConfigured) {
        d->ocStatus = OwncloudSettings::Error;
        d->ocError = OwncloudSettings::NoConfigurationError;
    } else {
        loadFolders();
        qCDebug(POC_SYNC) << "POC We're good" << (d->ocStatus != OwncloudSettings::Disconnected) << d->ocStatus;
    }
    emit statusChanged(d->ocStatus);
    emit errorChanged(d->ocError);
    emit owncloudChanged(d->owncloudInfo);
}

void CloudSync::loadFolders()
{
    if (d->ocStatus == OwncloudSettings::Connected) {
        qCDebug(POC_SYNC) << "connected" << d->folderMan->map();
        QStringList fs;
        foreach (OCC::Folder* f, d->folderMan->map()) {
            qCDebug(POC_SYNC) << "OC New folder: " << f->alias() << f->path() << f->remotePath();
            //fs << f->alias();
            connect(f, &OCC::Folder::syncFinished,
                    this, &CloudSync::folderSyncFinished);
            updateFolder(f);
        }
    } else {
        qCDebug(POC_SYNC) << "disco";
        d->folderList.clear();
        d->folders.clear();
        emit folderListChanged(d->folderList);
    }
}

void CloudSync::folderSyncFinished(const OCC::SyncResult &r)
{
    if (r.status() == OCC::SyncResult::Success) {
        OCC::Folder *f = static_cast<OCC::Folder*>(sender());
        if (f) {
            d->syncTime[f->alias()] = QDateTime::currentDateTime();
            qCDebug(POC_SYNC) << "Updating syncTime for " << f->alias() << d->syncTime[f->alias()];
        } else {
            //qCDebug(POC_SYNC) << " OC no folder found";
        }
    }
}

QString errorMsg(int r) {
    QString s;
    if (r == OCC::SyncResult::Success) s = "OCC::SyncResult::Success -> OwncloudFolder::Idle";
    if (r == OCC::SyncResult::Error) s = "OCC::SyncResult::Error -> OwncloudFolder::Error";
    if (r == OCC::SyncResult::NotYetStarted) s = "OCC::SyncResult::NotYetStarted -> OwncloudFolder::Waiting";
    if (r == OCC::SyncResult::SyncRunning) s = "OCC::SyncResult::SyncRunning -> OwncloudFolder::Running";
    if (r == OCC::SyncResult::SetupError) s = "OCC::SyncResult::SetupError -> OwncloudFolder::Error";
    if (r == OCC::SyncResult::Undefined) s = "OCC::SyncResult::Undefined -> OwncloudFolder::Error";
    return s;
}

void CloudSync::updateFolder(const OCC::Folder* folder)
{
    QVariantMap m;
    m["name"] = folder->alias();
    m["localPath"] = folder->path();
    m["remotePath"] = folder->remotePath();
    m["syncTime"] = d->syncTime[folder->alias()].toMSecsSinceEpoch();
//     qCDebug(POC_SYNC) << "OC updateFolder:: path, secondPath: " << folder->path() << ", " << d->syncTime[folder->alias()];

    int s = 999;
    int r = folder->syncResult().status();
    //qCDebug(POC_SYNC) << " c" << c << " r" << r;
    if (!folder->syncPaused()) {
        if (r == OCC::SyncResult::Success) s = OwncloudFolder::Idle;
        if (r == OCC::SyncResult::Error) s = OwncloudFolder::Error;
        if (r == OCC::SyncResult::NotYetStarted) s = OwncloudFolder::Waiting;
        if (r == OCC::SyncResult::SyncRunning) s = OwncloudFolder::Running;
        if (r == OCC::SyncResult::SetupError) s = OwncloudFolder::Error;
        if (r == OCC::SyncResult::Undefined) s = OwncloudFolder::Error;
    } else {
        s = OwncloudFolder::Disabled;
    }
    if (r == 999) {
        s = OwncloudFolder::Error;
    }
    m["status"] = s;

    if (s == OwncloudFolder::Error) {
        m["errorMessage"] = folder->syncResult().errorString();
    } else {
        m["errorMessage"] = QString();
    }

    d->folders[folder->alias()] = m;
    //qCDebug(POC_SYNC) << " OC FOLdERs: " << folder->alias() << folder->syncResult().statusString();
    emit folderChanged(m);
}

void CloudSync::addSyncFolder(const QString& localFolder, const QString& remoteFolder, const QString& alias)
{
    auto fd = OCC::FolderDefinition();
    fd.localPath = localFolder;
    fd.targetPath = remoteFolder;
    fd.alias = alias;

    auto acl = AccountManager::instance()->accounts();
    if (acl.count() == 0) {
        return;
    }
    if (acl.count() > 1) {
        qWarning() << "Application is not fit to use multiple account!";
    }
    AccountState* ac = acl.at(0).data();
    d->folderMan->addFolder(ac, fd);

    qCDebug(POC_SYNC) << "POC OCD OwncloudSyncDaemon::addSyncFolder: " << ac << localFolder << remoteFolder << alias;

    d->folderMan->setupFolders();
    refresh();
}

void CloudSync::removeSyncFolder(const QString& alias)
{
    //d->folderMan->slotRemoveFolder(alias); // Folder*

    qCDebug(POC_SYNC) << "OCD OwncloudSyncDaemon::removeSyncFolder: " << alias;

    if (!d->delay) {
        d->delay = new QTimer(this);
        d->delay->setSingleShot(true);
        d->delay->setInterval(2000);
        connect(d->delay, SIGNAL(timeout()), SLOT(delayedReadConfig()));
    }
    d->delay->start();
    d->folders.remove(alias);
    d->folderMan->setupFolders();
    refresh();
}

void CloudSync::delayedReadConfig()
{
    d->folderMan->setupFolders();
    refresh();
}

void CloudSync::checkRemoteFolder(const QString& f)
{
    if (d->ocStatus == OwncloudSettings::Connected) {
        qWarning() << "checkRemoteFolder needs porting";
//         QNetworkReply* reply = d->ocInfo->getDirectoryListing(f);
//         connect(d->ocInfo, SIGNAL(directoryListingUpdated(const &QStringList)),
//                 this, SLOT(slotDirectoryListingUpdated(const QStringList&)));
//         //QNetworkReply* reply = d->ocInfo->getWebDAVPath(f);
//         connect(reply, SIGNAL(finished()), SLOT(slotCheckRemoteFolderFinished()));
//         //connect(reply, SIGNAL(finished()), SLOT(slotCheckRemoteFolderFinished()));
    }
}

void CloudSync::slotDirectoryListingUpdated(const QStringList &directories )
{
    qCDebug(POC_SYNC) << "POC found direvotories" << directories;
}


void CloudSync::slotCheckRemoteFolderFinished()
{
    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    bool exists = reply->error() == QNetworkReply::NoError;
    QString p = reply->url().toString().split("remote.php/webdav/")[1];
    emit remoteFolderExists(p, exists);
}

void CloudSync::slotDirCheckReply(const QString &url, QNetworkReply *reply)
{
    //qCDebug(POC_SYNC) << " OC slotDirCheckReply" << url <<  (reply->error() == QNetworkReply::NoError) << (int)(reply->error());
    emit remoteFolderExists(url, reply->error() == QNetworkReply::NoError);
}

void CloudSync::createRemoteFolder(const QString &f)
{
    if(f.isEmpty()) return;

//     qCDebug(POC_SYNC) << "OC creating folder on ownCloud: " << f;
//     QNetworkReply* reply = d->ocInfo->mkdirRequest(f);
//     connect(reply, SIGNAL(finished()), SLOT(slotCreateRemoteFolderFinished()));
//     //connect(reply, SIGNAL(error()), SLOT(slotCreateRemoteFolderFinished()));

}

void CloudSync::slotCreateRemoteFolderFinished()
{
    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    bool exists = reply->error() == QNetworkReply::NoError;
    QString p = reply->url().toString().split("remote.php/webdav/")[1];
    qCDebug(POC_SYNC) << " === OC slot -- CREATE -- RemoteFolderFinished() : " << reply->url() << reply->error() << p << exists;
    emit remoteFolderExists(p, exists);
}

void CloudSync::slotOwnCloudFound( const QString& url, const QString& versionStr, const QString& version, const QString& edition)
{
    qCDebug(POC_SYNC) << "OCD : ownCloud found: " << url << " with version " << versionStr << "(" << version << ")";
    // now check the authentication

    OCC::ConfigFile cfgFile;
    //cfgFile.setOwnCloudVersion( version );
    qCDebug(POC_SYNC) << " OC polling interval: " << cfgFile.remotePollInterval();

    d->owncloudInfo["url"] = url;
    d->owncloudInfo["version"] = version;
    d->owncloudInfo["versionString"] = versionStr;
    d->owncloudInfo["edition"] = edition;

//     d->ocStatus = OwncloudSettings::Disconnected;
//     d->ocError = OwncloudSettings::NoError;

//     emit owncloudChanged(d->owncloudInfo);
//     emit statusChanged(d->ocStatus);
//     emit errorChanged(d->ocError);

}

void CloudSync::slotFetchCredentials()
{
//     init();
    Accounts::AccountIdList accounts = KAccounts::accountsManager()->accountListEnabled(QStringLiteral("dav-contacts"));
    qCDebug(POC_SYNC) << "Accounts: " << accounts;

    Q_FOREACH (const quint32 accountId, accounts) {
        getCredentials(accountId);
    }

    /* FIXME: credentials
    connect( OCC::CredentialStore::instance(), SIGNAL(fetchCredentialsFinished(bool)),
                this, SLOT(slotCredentialsFetched(bool)) );
    qCDebug(POC_SYNC) << " ======= fetchCredentials()";
    OCC::CredentialStore::instance()->fetchCredentials();
    */
}

void CloudSync::getCredentials(const Accounts::AccountId accountId)
{
    GetCredentialsJob *credentialsJob = new GetCredentialsJob(accountId, this);
    connect(credentialsJob, &GetCredentialsJob::finished, this, [=](KJob *job) {
            qCDebug(POC_SYNC) << "Cred job returns" << job->error();
            GetCredentialsJob *credentialsJob = qobject_cast<GetCredentialsJob*>(job);
            job->deleteLater();

            if (!credentialsJob) {
                return;
            }

            const QVariantMap &data = credentialsJob->credentialsData();
            Accounts::Account *account = KAccounts::accountsManager()->account(credentialsJob->accountId());

            if (!account) {
                return;
            }

            QUrl carddavUrl = account->value("carddavUrl").toUrl();
            const QString &userName = data.value("AccountUsername").toString();

            qCDebug(POC_SYNC) << "Using: host:" << carddavUrl.host() << "path:" << carddavUrl;
            qCDebug(POC_SYNC) << "Account ID: " << credentialsJob->accountId();
            qCDebug(POC_SYNC) << "Server: " << account->value(QStringLiteral("server")).toString();
            qCDebug(POC_SYNC) << "UserName: " << userName;
            qCDebug(POC_SYNC) << "Secret: " << data.value("Secret").toString();
            qCDebug(POC_SYNC) << "ACcco  :" << account->allKeys();
            Q_FOREACH (auto k, account->allKeys()) {
                qCDebug(POC_SYNC) << "Acccount[" << k << "]:" << account->value(k);
            }
            //init();
        }
    );
    credentialsJob->start();
}

void CloudSync::slotCredentialsFetched(bool ok)
{
    /*
    qCDebug(POC_SYNC) << "OC Credentials successfully fetched: " << ok;
    QString trayMessage;
    if( ! ok ) {
        trayMessage = tr("Error: Could not retrieve the password!");
        trayMessage = OCC::CredentialStore::instance()->errorMessage();
        if( !trayMessage.isEmpty() ) {
            //_tray->showMessage(tr("Credentials"), trayMessage);
        }

        qCDebug(POC_SYNC) << "OC Could not fetch credentials" << trayMessage;
    } else {
        // Credential fetched ok.
        const QString _url = d->owncloudInfo["url"].toString();
        const QString _u = OCC::CredentialStore::instance()->user();
        const QString _p = OCC::CredentialStore::instance()->password();
        qCDebug(POC_SYNC) << "OC User / password === " << _u << _p << _url;
//         qCDebug(POC_SYNC) << "OC slotCredentialsFetched" << trayMessage;
        if(d->ocInfo->isConfigured() ) {
            d->ocInfo->checkInstallation();
            loadFolders();
        }
        d->ocInfo->setCredentials(_u, _p);
        QTimer::singleShot(0, this, SLOT(slotCheckAuthentication()));
    }
    disconnect( OCC::CredentialStore::instance(), SIGNAL(fetchCredentialsFinished(bool)) );
    */
}

void CloudSync::slotNoOwnCloudFound(QNetworkReply* reply)
{
    Q_UNUSED(reply)
    d->ocStatus = OwncloudSettings::Error;
    d->ocError = OwncloudSettings::NoConfigurationError;
    qCDebug(POC_SYNC) << "POC : slotNoOwnCloudFound";
    emit statusChanged(d->ocStatus);
    emit errorChanged(d->ocError);
}

void CloudSync::slotCheckAuthentication()
{
    qCDebug(POC_SYNC) << "OwncloudSync::slotCheckAuthentication()";
//     //QNetworkReply *reply = d->ocInfo->getDirectoryListing(QString::fromLatin1("/")); // this call needs to be authenticated.
//     QNetworkReply *reply = d->ocInfo->getRequest(d->owncloudInfo["url"].toString());
//
//     connect(reply, SIGNAL(finished()), this, SLOT(slotAuthCheck()));
}

void CloudSync::slotAuthCheck()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    if( reply->error() == QNetworkReply::AuthenticationRequiredError ) { // returned if the user is wrong.
        if (d->ocStatus != OwncloudSettings::Error ||
                            d->ocError != OwncloudSettings::AuthenticationError) {
            qCDebug(POC_SYNC) << "OC ******** Password is wrong!";
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::AuthenticationError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    } else if( reply->error() == QNetworkReply::OperationCanceledError ) {
        // the username was wrong and ownCloudInfo was closing the request after a couple of auth tries.
        qCDebug(POC_SYNC) << "OC ******** Username or password is wrong!";
        if (d->ocStatus != OwncloudSettings::Error ||
                            d->ocError != OwncloudSettings::AuthenticationError) {
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::AuthenticationError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    } else {
//         qCDebug(POC_SYNC) << "OC ######## Credentials are ok!";
        qCDebug(POC_SYNC) << "POC CHECK statusChanged connected?" << (d->ocStatus == OwncloudSettings::Connected);
        if (d->ocStatus != OwncloudSettings::Connected) {
            qCDebug(POC_SYNC) << "OC ****** changing to Connected/NoError! :-)";
            d->ocStatus = OwncloudSettings::Connected;
            d->ocError = OwncloudSettings::NoError;
            qCDebug(POC_SYNC) << "________ POC emit statusChanged " << (d->ocStatus == OwncloudSettings::Connected);
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
            d->folderMan->setupFolders();
            loadFolders();
        }
    }
}

void CloudSync::accountStateChanged(int state)
{
    //auto account = dynamic_cast<AccountStatePtr>(sender());

    if (state == AccountState::Connected) {
        qCDebug(POC_SYNC) << "HOoooray!";
        if (d->ocStatus != OwncloudSettings::Connected) {

            qCDebug(POC_SYNC) << "OC ****** changing to Connected/NoError! :-)";
            d->ocStatus = OwncloudSettings::Connected;
            d->ocError = OwncloudSettings::NoError;
            qCDebug(POC_SYNC) << "________ POC emit statusChanged " << (d->ocStatus == OwncloudSettings::Connected);
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
            d->folderMan->setupFolders();
            loadFolders();
        }
    } else if (state == AccountState::ConfigurationError) {
        if (d->ocStatus != OwncloudSettings::Error ||
            d->ocError != OwncloudSettings::AuthenticationError) {

            qCDebug(POC_SYNC) << "OC ******** Likely, password is wrong!";
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::AuthenticationError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    } else if (state == AccountState::Disconnected) {
        if (d->ocStatus != OwncloudSettings::Disconnected) {
            d->ocStatus = OwncloudSettings::Disconnected;
            d->ocError = OwncloudSettings::NoError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
            //
    } else if (state == AccountState::NetworkError || state == AccountState::ServiceUnavailable) {
        if (d->ocStatus != OwncloudSettings::Error ||
            d->ocError != OwncloudSettings::NetworkError) {

            qCDebug(POC_SYNC) << "OC ******** NetworkError or ServiceUnavailable!";
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::NetworkError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    //} else if (state == AccountState::ServiceUnavailable) {

    } else {
        qWarning() << "I don't know how to handle this state." << state;

    }
}



void CloudSync::setupOwncloud(const QString &server, const QString &user, const QString &password)
{

//     //OCC::ConfigFile cfgFile(d->configHandle);
//     OCC::ConfigFile cfgFile();
//     cfgFile.setRemotePollInterval(600000); // ten minutes for now
//
//     bool https = server.startsWith("https");
//
//     QString _srv = server;
//     if (!server.endsWith('/')) {
//         _srv.append('/');
//     }
//
//     cfgFile.writeOwncloudConfig(QLatin1String("ownCloud"), _srv, user, password);
//     OCC::CredentialStore::instance()->saveCredentials();
//     d->ocInfo->setCredentials(user, password); // add server
//     qCDebug(POC_SYNC) << "OC - - - - -  Setting up OwnCloud: " << _srv << user << password << https;
//     cfgFile.acceptCustomConfig();
//
//     if( d->folderMan ) {
//         d->folderMan->removeAllFolderDefinitions();
//     }
//
//     d->configHandle.clear();
//     d->ocInfo->setCustomConfigHandle(QString());
//
//     d->ocInfo->setCustomConfigHandle(d->configHandle);
//     if (d->ocInfo->isConfigured()) {
//         // reset the SSL Untrust flag to let the SSL dialog appear again.
//         d->ocInfo->resetSSLUntrust();
//         d->ocInfo->checkInstallation();
//         loadFolders();
//     } else {
//         qCDebug(POC_SYNC) << " OC  ownCloud seems not configured.";
//     }
    QTimer::singleShot(0, this, SLOT(slotCheckAuthentication()));
}

void CloudSync::slotGuiLog(const QString& err, const QString& msg)
{
    qCDebug(POC_SYNC) << "guilog() POC" << err << msg;
}



#include "cloudsync.moc"

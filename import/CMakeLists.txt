
set(cloudsyncimport_SRCS
    cloudsyncimport.cpp
    ../lib/owncloudsettings.cpp
    ../lib/owncloudfolder.cpp
    ../lib/directorylister.cpp
    ../lib/dirmodel.cpp
    ../lib/minijob.cpp
    ../lib/job.cpp
    ../lib/createfolderjob.cpp
    ../lib/syncprogress.cpp
)

qt5_add_dbus_interface(cloudsyncimport_SRCS ../daemon/org.kde.cloudsync.xml owncloud_interface )

add_library(cloudsyncimport SHARED ${cloudsyncimport_SRCS})
target_link_libraries(cloudsyncimport
        Qt5::DBus
        Qt5::Quick
        Qt5::Qml
        KF5::I18n
        KF5::KIOCore
        KF5::KIOWidgets
)

#install(DIRECTORY qml/ DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/components)

message("DESTINATION ${KDE_INSTALL_QMLDIR}")

install(TARGETS cloudsyncimport DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/owncloud)
install(FILES qmldir DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/owncloud)
install(DIRECTORY qml/ DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/owncloud)

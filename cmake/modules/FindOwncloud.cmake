# Copyright 2015 Sebastian Kügler <sebas@kde.org>
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING* file.

# - Try to find libocsync, libowncloudsync, libowncloudclient
# Once done this will define
#  OWNCLOUD_FOUND - System has owncloud libs and headers
#  OWNCLOUD_INCLUDES - The necessary include directories
#  OWNCLOUD_LIBRARIES - The libraries needed to use owncloud libs

find_path(OCSYNC_INCLUDE_DIR
            NAMES
              csync.h
            PATH_SUFFIXES
              ocsync
            )

# message("FIND csync include ${OCSYNC_INCLUDE_DIR}")
message("oscyns .... ${CMAKE_LIBRARY_ARCHITECTURE} -- ${CMAKE_LIBRARY_ARCHITECTURE} -- ${CMAKE_INSTALL_PREFIX}/lib/${CMAKE_LIBRARY_ARCHITECTURE}/owncloud")
find_library(OCSYNC_LIBRARY
            NAMES
              ocsync
            PATHS
               /usr/lib/owncloud
               /usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}/owncloud
               /usr/local/lib/owncloud
               /opt/local/lib/owncloud
               ${CMAKE_LIBRARY_PATH}/owncloud
               #${CMAKE_LIBRARY_PATH}/${CMAKE_LIBRARY_ARCHITECTURE}/owncloud
               ${CMAKE_INSTALL_PREFIX}/lib/${CMAKE_LIBRARY_ARCHITECTURE}/owncloud
            )

find_path(OWNCLOUDSYNC_INCLUDE_DIR
          NAMES
              owncloudlib.h
          PATH_SUFFIXES
              owncloudsync
            )

set(OWNCLOUDSYNC_INCLUDE_DIRS ${OWNCLOUDSYNC_INCLUDE_DIR}
                              ${OWNCLOUDSYNC_INCLUDE_DIR}/mirall)


# message("FIND sync include ${OWNCLOUDSYNC_INCLUDE_DIRS}")

find_library(OWNCLOUDSYNC_LIBRARY
            NAMES
              owncloudsync
            PATHS
               /usr/lib
               /usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
               /usr/local/lib
               /opt/local/lib
               ${CMAKE_LIBRARY_PATH}
               ${CMAKE_INSTALL_PREFIX}/lib
            )



find_path(OWNCLOUDCLIENT_INCLUDE_DIR
            NAMES
              accountmanager.h
            PATH_SUFFIXES
              owncloudclient
            )
set(OWNCLOUDCLIENT_INCLUDE_DIRS ${OWNCLOUDCLIENT_INCLUDE_DIR}
                                ${OWNCLOUDCLIENT_INCLUDE_DIR}/mirall)

# message("FIND client include ${OWNCLOUDCLIENT_INCLUDE_DIR}")

find_library(OWNCLOUDCLIENT_LIBRARY
            NAMES
              owncloudclient
            PATHS
               /usr/lib
               /usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}
               /usr/local/lib
               /opt/local/lib
               ${CMAKE_LIBRARY_PATH}
               ${CMAKE_INSTALL_PREFIX}/lib
            )



set(OWNCLOUD_LIBRARIES ${OCSYNC_LIBRARY} ${OWNCLOUDSYNC_LIBRARY} ${OWNCLOUDCLIENT_LIBRARY})
set(OWNCLOUD_INCLUDES ${OCSYNC_INCLUDE_DIR} ${OWNCLOUDSYNC_INCLUDE_DIRS} ${OWNCLOUDCLIENT_INCLUDE_DIRS})

set(OWNCLOUD_FOUND false)
if (OCSYNC_FOUND AND OWNCLOUDSYNC_FOUND AND OWNCLOUDCLIENT_FOUND)
    set(OWNCLOUD_FOUND true)
endif()

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set OWNCLOUDCLIENT_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Owncloud  DEFAULT_MSG
    OWNCLOUD_LIBRARIES OWNCLOUD_INCLUDES)

mark_as_advanced(OWNCLOUD_INCLUDES OWNCLOUD_LIBRARIES)
